<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Dictionary;

/**
 * Dictionary of default config values.
 *
 * @author Kieran Adler <kdadler@gmail.com>
 */
class Config
{
    public const PHP_DOT_NET_URL = 'php_dot_net_url';
    public const BASE_VERSION = 'base_version';
    public const CHECK_ENV = 'check_env';
    public const DOCKERFILES = 'dockerfiles';
    public const DOCKER_CONTAINERS = 'docker_containers';
    public const DEFAULTS = [
        self::PHP_DOT_NET_URL => 'https://www.php.net/releases/?json',
        self::CHECK_ENV => true,
        self::BASE_VERSION => null,
        self::DOCKERFILES => [],
        self::DOCKER_CONTAINERS => [],
    ];
}
