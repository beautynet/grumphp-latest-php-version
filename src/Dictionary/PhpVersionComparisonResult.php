<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Dictionary;

/**
 * Dictionary of comparison result values.
 *
 * @author Kieran Adler <kdadler@gmail.com>
 */
class PhpVersionComparisonResult
{
    public const EARLIER = 2;
    public const LATER = 4;
    public const MAJOR_MISMATCH = 0;
    public const MINOR_MISMATCH = 1;
    public const MATCH = 3;
}
