<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Container;

/**
 * PHP version container.
 */
readonly class PhpVersion
{
    public ?int $major;
    public ?int $minor;
    public ?int $patch;

    public function __construct(string $version)
    {
        $digits = explode('.', $version);

        $this->major = array_key_exists(0, $digits) ? (int) ($digits[0]) : null;
        $this->minor = array_key_exists(1, $digits) ? (int) ($digits[1]) : null;
        $this->patch = array_key_exists(2, $digits) ? (int) ($digits[2]) : null;
    }

    public function format(): string
    {
        $digits = [];

        if ($this->major !== null) {
            $digits[] = $this->major;
        }
        if ($this->minor !== null) {
            $digits[] = $this->minor;
        }
        if ($this->patch !== null) {
            $digits[] = $this->patch;
        }

        return implode('.', $digits);
    }
}
