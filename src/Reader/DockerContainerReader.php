<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Reader;

use Beautynet\GrumphpLatestPhpVersion\Util\Cli;

/**
 * Reads the version number from a docker container.
 */
class DockerContainerReader
{
    public function __construct(private readonly Cli $cli)
    {
    }

    public function read(string $containerName): string
    {
        $lines = $this->cli->runCommand(sprintf("docker exec %s /bin/bash -c 'php --version'", $containerName));
        $output = reset($lines);

        if (str_contains($output, 'Error: No such container')) {
            throw new \RuntimeException($output);
        }

        return $output;
    }
}
