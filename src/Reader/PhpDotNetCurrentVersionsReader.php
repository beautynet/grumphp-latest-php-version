<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Reader;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;
use Beautynet\GrumphpLatestPhpVersion\Util\HttpRequest;

/**
 * Reads the current versions data from php.net.
 */
class PhpDotNetCurrentVersionsReader
{
    public function __construct(private readonly HttpRequest $httpRequest)
    {
    }

    /**
     * @param string $phpDotNetUrl
     * @param PhpVersion|null $baseVersion
     *
     * @return array
     */
    public function read(string $phpDotNetUrl, ?PhpVersion $baseVersion): array
    {
        $url = $baseVersion ? sprintf('%s&version=%s', $phpDotNetUrl, $baseVersion->format()) : $phpDotNetUrl;
        $response = $this->httpRequest->get($url);

        try {
            $decodedResponse = json_decode($response, true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException) {
            throw new \RuntimeException('Failed to read current version JSON data from php.net');
        }

        if ($decodedResponse === ['error' => 'Unknown version']) {
            throw new \UnexpectedValueException('Malformed base_version value provided. This must be a valid PHP version');
        }

        return $decodedResponse;
    }
}
