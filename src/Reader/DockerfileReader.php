<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Reader;

/**
 * Reads the content of a Dockerfile.
 */
class DockerfileReader
{
    public function read(string $fileName): string
    {
        $content = file_get_contents($fileName);

        if (!is_string($content)) {
            throw new \RuntimeException("Dockerfile '$fileName' not found");
        }

        return $content;
    }
}
