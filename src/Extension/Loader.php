<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Extension;

use GrumPHP\Extension\ExtensionInterface;

/**
 * Custom GrumPHP extension.
 */
class Loader implements ExtensionInterface
{
    public function imports(): iterable
    {
        yield __DIR__ . '/../Resources/config/services.yaml';
    }
}
