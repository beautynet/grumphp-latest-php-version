<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Comparison;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;
use Beautynet\GrumphpLatestPhpVersion\Dictionary\PhpVersionComparisonResult;

/**
 * Service that compares two PHP versions.
 */
class PhpVersionComparisonService
{
    public function compare(PhpVersion $a, PhpVersion $b): int
    {
        if ($a->major !== $b->major) {
            return PhpVersionComparisonResult::MAJOR_MISMATCH;
        }
        if ($a->minor !== $b->minor) {
            return PhpVersionComparisonResult::MINOR_MISMATCH;
        }
        if ($a->patch < $b->patch) {
            return PhpVersionComparisonResult::EARLIER;
        }

        return $a->patch > $b->patch ? PhpVersionComparisonResult::LATER : PhpVersionComparisonResult::MATCH;
    }
}
