<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Util;

/**
 * Wrapper around the exec construct.
 */
class Cli
{
    public function runCommand(string $command): array
    {
        $lines = [];
        exec(sprintf('%s 2>&1', $command), $lines);

        return $lines;
    }
}
