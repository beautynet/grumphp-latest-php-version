<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Util;

/**
 * Curl wrapper class.
 */
class HttpRequest
{
    /**
     * Makes a request.
     *
     * This is basic as per requirements.
     */
    public function get(string $url): string
    {
        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);

        $response = (string) curl_exec($handle);

        curl_close($handle);

        return $response;
    }
}
