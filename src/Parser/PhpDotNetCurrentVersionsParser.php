<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Parser;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;

/**
 * Parses the current versions read from the php.net site.
 */
class PhpDotNetCurrentVersionsParser
{
    public function parse(array $data): array
    {
        $versions = [];

        if (array_key_exists('version', $data)) {
            $versions[] = new PhpVersion($data['version']);
        } else {
            foreach ($data as $entry) {
                $versions[] = new PhpVersion($entry['version'] ?? '');
            }
        }

        return $versions;
    }
}
