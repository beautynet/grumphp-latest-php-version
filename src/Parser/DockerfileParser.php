<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Parser;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;

/**
 * Parses a Dockerfile's content looking for a PHP version.
 */
class DockerfileParser
{
    public function parse(string $fileName, string $fileContent): ?PhpVersion
    {
        $line = $this->getLine($fileContent);
        $version = null;

        if ($line === null) {
            throw new \UnexpectedValueException("PHP container definition line not found in Dockerfile $fileName");
        }

        $versionMatches = [];
        preg_match('/\d+\.\d+\.\d+/', $line, $versionMatches);

        if (count($versionMatches) > 0) {
            $version = new PhpVersion(reset($versionMatches));
        }

        return $version;
    }

    private function getLine(string $fileContent): ?string
    {
        foreach (explode(PHP_EOL, $fileContent) as $line) {
            if (preg_match('/php:(.)*/', $line)) {
                return $line;
            }
        }

        return null;
    }
}
