<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Parser;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;

/**
 * Parses the PHP version read from a Docker container.
 */
class DockerContainerParser
{
    public function parse(string $containerName, string $content): ?PhpVersion
    {
        $matches = [];
        preg_match('/PHP\s\d+\.\d+\.\d+/', $content, $matches);

        if (count($matches) === 0) {
            throw new \RuntimeException("Container $containerName is not running PHP");
        }

        return new PhpVersion(str_replace('PHP ', '', reset($matches)));
    }
}
