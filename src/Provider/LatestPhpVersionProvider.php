<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Provider;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;
use Beautynet\GrumphpLatestPhpVersion\Dictionary\Config;
use Beautynet\GrumphpLatestPhpVersion\Parser\PhpDotNetCurrentVersionsParser;
use Beautynet\GrumphpLatestPhpVersion\Reader\PhpDotNetCurrentVersionsReader;

/**
 * Provides the latest version for the given base version.
 */
class LatestPhpVersionProvider
{
    public function __construct(
        private readonly PhpDotNetCurrentVersionsReader $reader,
        private readonly PhpDotNetCurrentVersionsParser $parser,
    ) {
    }

    /**
     * @param array $options
     * @param PhpVersion|null $baseVersion
     *
     * @return PhpVersion|null
     */
    public function provide(array $options, ?PhpVersion $baseVersion): ?PhpVersion
    {
        $foundVersions = $this->parser->parse($this->reader->read($options[Config::PHP_DOT_NET_URL], $baseVersion));
        $version = null;

        if ($baseVersion) {
            foreach ($foundVersions as $foundVersion) {
                if (
                    $foundVersion->major === $baseVersion->major
                    && ($foundVersion->minor === $baseVersion->minor || $baseVersion->minor === null)
                ) {
                    $version = $foundVersion;
                    break;
                }
            }
        } else {
            $version = array_reduce(
                $foundVersions,
                function (?PhpVersion $carry, PhpVersion $version) {
                    return $carry === null || $carry->major < $version->major ? $version : $carry;
                },
                null,
            );
        }

        return $version;
    }
}
