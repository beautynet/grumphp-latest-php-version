<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Provider;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;

/**
 * Provides the CLI PHP version.
 */
class EnvironmentPhpVersionProvider
{
    public function provide(): PhpVersion
    {
        return new PhpVersion(PHP_VERSION);
    }
}
