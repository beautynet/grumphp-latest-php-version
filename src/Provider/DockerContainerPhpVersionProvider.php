<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Provider;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;
use Beautynet\GrumphpLatestPhpVersion\Parser\DockerContainerParser;
use Beautynet\GrumphpLatestPhpVersion\Reader\DockerContainerReader;

/**
 * Docker container PHP version provider.
 */
class DockerContainerPhpVersionProvider
{
    public function __construct(
        private readonly DockerContainerReader $reader,
        private readonly DockerContainerParser $parser,
    ) {
    }

    public function provide(string $containerName): ?PhpVersion
    {
        return $this->parser->parse($containerName, $this->reader->read($containerName));
    }
}
