<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Provider;

use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;
use Beautynet\GrumphpLatestPhpVersion\Parser\DockerfileParser;
use Beautynet\GrumphpLatestPhpVersion\Reader\DockerfileReader;

/**
 * Provides the PHP version from a Dockerfile.
 */
class DockerfilePhpVersionProvider
{
    public function __construct(private readonly DockerfileReader $reader, private readonly DockerfileParser $parser)
    {
    }

    public function provide(string $fileName): ?PhpVersion
    {
        return $this->parser->parse($fileName, $this->reader->read($fileName));
    }
}
