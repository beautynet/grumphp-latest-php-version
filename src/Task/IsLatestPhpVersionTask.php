<?php

declare(strict_types=1);

namespace Beautynet\GrumphpLatestPhpVersion\Task;

use Beautynet\GrumphpLatestPhpVersion\Comparison\PhpVersionComparisonService;
use Beautynet\GrumphpLatestPhpVersion\Container\PhpVersion;
use Beautynet\GrumphpLatestPhpVersion\Dictionary\Config;
use Beautynet\GrumphpLatestPhpVersion\Dictionary\PhpVersionComparisonResult;
use Beautynet\GrumphpLatestPhpVersion\Provider\DockerContainerPhpVersionProvider;
use Beautynet\GrumphpLatestPhpVersion\Provider\DockerfilePhpVersionProvider;
use Beautynet\GrumphpLatestPhpVersion\Provider\EnvironmentPhpVersionProvider;
use Beautynet\GrumphpLatestPhpVersion\Provider\LatestPhpVersionProvider;
use GrumPHP\Runner\TaskResult;
use GrumPHP\Runner\TaskResultInterface;
use GrumPHP\Task\Config\ConfigOptionsResolver;
use GrumPHP\Task\Config\EmptyTaskConfig;
use GrumPHP\Task\Config\TaskConfigInterface;
use GrumPHP\Task\Context\ContextInterface;
use GrumPHP\Task\Context\GitPreCommitContext;
use GrumPHP\Task\Context\RunContext;
use GrumPHP\Task\TaskInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Task to execute.
 */
class IsLatestPhpVersionTask implements TaskInterface
{
    private readonly TaskConfigInterface $config;

    public function __construct(
        private readonly LatestPhpVersionProvider $latestPhpVersionProvider,
        private readonly EnvironmentPhpVersionProvider $environmentPhpVersionProvider,
        private readonly DockerfilePhpVersionProvider $dockerfilePhpVersionProvider,
        private readonly DockerContainerPhpVersionProvider $dockerContainerPhpVersionProvider,
        private readonly PhpVersionComparisonService $comparisonService,
        ?TaskConfigInterface $config = null
    ) {
        $this->config = $config ?? new EmptyTaskConfig();
    }

    public static function getConfigurableOptions(): ConfigOptionsResolver
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            Config::PHP_DOT_NET_URL => Config::DEFAULTS[Config::PHP_DOT_NET_URL],
            Config::CHECK_ENV => Config::DEFAULTS[Config::CHECK_ENV],
            Config::BASE_VERSION => Config::DEFAULTS[Config::BASE_VERSION],
            Config::DOCKERFILES => Config::DEFAULTS[Config::DOCKERFILES],
            Config::DOCKER_CONTAINERS => Config::DEFAULTS[Config::DOCKER_CONTAINERS],
        ]);

        return ConfigOptionsResolver::fromClosure(fn ($options) => $resolver->resolve($options));
    }

    public function canRunInContext(ContextInterface $context): bool
    {
        return $context instanceof RunContext || $context instanceof GitPreCommitContext;
    }

    public function getConfig(): TaskConfigInterface
    {
        return $this->config;
    }

    public function withConfig(TaskConfigInterface $config): TaskInterface
    {
        return new self(
            $this->latestPhpVersionProvider,
            $this->environmentPhpVersionProvider,
            $this->dockerfilePhpVersionProvider,
            $this->dockerContainerPhpVersionProvider,
            $this->comparisonService,
            $config,
        );
    }

    public function run(ContextInterface $context): TaskResultInterface
    {
        $options = $this->config->getOptions();
        $baseVersion = null;

        if ($options[Config::BASE_VERSION] ?? null) {
            $baseVersion = new PhpVersion((string) $options[Config::BASE_VERSION]);
        }

        $latestVersion = $this->latestPhpVersionProvider->provide($options, $baseVersion);

        if ($options[Config::CHECK_ENV]) {
            $envVersion = $this->environmentPhpVersionProvider->provide();

            if ($latestVersion === null) {
                return TaskResult::createFailed($this, $context, 'Version not read from php.net');
            }

            $error = $this->test($envVersion, $latestVersion, 'environment');
            if ($error) {
                return TaskResult::createFailed($this, $context, $error);
            }
        }

        foreach ($options[Config::DOCKERFILES] ?? [] as $fileName) {
            $dockerfileVersion = $this->dockerfilePhpVersionProvider->provide($fileName);

            if ($dockerfileVersion === null) {
                continue;
            }

            $error = $this->test($dockerfileVersion, $latestVersion, "Dockerfile $fileName");
            if ($error) {
                return TaskResult::createFailed($this, $context, $error);
            }
        }

        foreach ($options[Config::DOCKER_CONTAINERS] ?? [] as $containerName) {
            $containerVersion = $this->dockerContainerPhpVersionProvider->provide($containerName);

            if ($containerVersion === null) {
                continue;
            }

            $error = $this->test($containerVersion, $latestVersion, "Docker container $containerName");
            if ($error) {
                return TaskResult::createFailed($this, $context, $error);
            }
        }

        return TaskResult::createPassed($this, $context);
    }

    private function test(PhpVersion $subject, PhpVersion $latestVersion, string $type): ?string
    {
        $envResult = $this->comparisonService->compare($subject, $latestVersion);
        $error = null;

        if ($envResult === PhpVersionComparisonResult::MAJOR_MISMATCH) {
            $error = sprintf(
                'PHP version mismatch: The %s version %s major number does not match the latest version %s. Have you provided the correct base version in the grumphp file?',
                $type,
                $subject->format(),
                $latestVersion->format(),
            );
        }
        if ($envResult === PhpVersionComparisonResult::MINOR_MISMATCH) {
            $error = sprintf(
                'PHP version mismatch: The %s version %s minor number does not match the latest version %s. Have you provided the correct base version in the grumphp file?',
                $type,
                $subject->format(),
                $latestVersion->format(),
            );
        }
        if ($envResult === PhpVersionComparisonResult::EARLIER) {
            $error = sprintf(
                'PHP version out of date: The %s version %s is older than the currently available version %s.',
                $type,
                $subject->format(),
                $latestVersion->format(),
            );
        }

        return $error;
    }
}
