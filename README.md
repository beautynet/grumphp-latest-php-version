# GrumPHP Latest PHP Version

Custom GrumPHP extension that reviews the project's PHP versions and alerts if they are not the latest versions.

## How It Works

This package contains a task that checks for up-to-date PHP versions in your project. It can check the following places:

* The version on the CLI where the git command is being executed
* The version specified in Dockerfiles
* The version currently running in docker containers

To do this, it reads the latest version information from php.net and compares the patch number to the versions found in
the above places. 

## How To Use

### 1. Install GrumPHP

How to do this: https://github.com/phpro/grumphp

### 2. Add the repository to your project's composer.json file

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "git@gitlab.com:beautynet/grumphp-latest-php-version.git"
    }
  ]
}
```

### 3. Add the version to the require-dev in the same composer.json file

```json
{
  "require-dev": {
    "beautynet/grumphp-latest-php-version": "^0.0"
  }
}
```


### 4. Run composer update

You know how to do this...

### 5. Update the project's grumphp.yml file

```yaml
grumphp:
    extensions:
        - Beautynet\GrumphpLatestPhpVersion\Extension\Loader
    tasks:
        is_latest_php_version: {}

```

DONE :D

## Configuration

```yaml
grumphp:
    extensions:
        - Beautynet\GrumphpLatestPhpVersion\Extension\Loader
    tasks:
        is_latest_php_version:
            php_dot_net_url: http://php.net/versions
            base_version: '7.4'
            check_env: true
            dockerfiles: [Dockerfile, build/Dockerfile.example]
            docker_containers: [example_app_container]
```

### php_dot_net_url

default: `https://www.php.net/releases/?json`

Allows the URL the versions are read from to be changed. Best to leave it alone though.

### base_version

default: `null`

The base PHP version that the project is using. Only the major and minor numbers should be provided, eg `7.4` or `5.6`.
This should be provided as a string, for example `base_version: '8.0'` not `base_version: 8.0`.

If left out, or set as null then the latest major/minor release will be assumed.

### check_env

default: `true`

Should the local CLI PHP version be checked?

### dockerfiles

default: `[]`

List of the Dockerfiles to check the versions for. This should be listed relative to the project's root where the 
git command will be run.

### docker_containers

default: `[]`

List of the docker container names to check the versions for. Non-running containers, or those not running PHP will
result in the task failing.

## TODO

* Current version read static cache
* Static code-quality analysis
* Unit tests
* Pipeline tests
